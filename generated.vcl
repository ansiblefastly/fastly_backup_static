pragma optional_param default_ssl_check_cert 1;
C!
W!
# Backends

backend F_addr_70_42_185_11 {
    .first_byte_timeout = 75s;
    .connect_timeout = 75s;
    .max_connections = 200;
    .between_bytes_timeout = 10s;
    .share_key = "6UFFiCnKbSDy34Yt6gOeGc";
    .port = "80";
    .host = "70.42.185.11";
  
      
    .probe = {
        .request = "HEAD /fastly_healthcheck.txt HTTP/1.1" "Host: idge.staticworld.net" "Connection: close" "User-Agent: Varnish/fastly (healthcheck)";
        .threshold = 1;
        .window = 2;
        .timeout = 15s;
        .initial = 1;
        .expected_response = 200;
        .interval = 60s;
      }
}


backend shield_cache_sjc3120_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.20";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3121_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.21";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3122_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.22";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3123_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.23";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3124_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.24";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3125_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.25";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3126_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.26";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3127_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.27";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3128_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.28";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3129_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.29";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3130_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.30";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3131_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.31";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3132_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.32";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3133_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.33";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3134_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.34";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}
backend shield_cache_sjc3135_SJC {
    .is_shield = true;
    .connect_timeout = 2s;
    .max_connections = 1000;
    .share_key = "fastlyshield";
    .port = "80";
    .host = "23.235.47.35";
  
      
    .probe = {
        .request = "HEAD /__varnish_shield_check HTTP/1.1";
        .window = 5;
        .threshold = 3;
        .timeout = 2s;
        .initial = 2;
        .interval = 1s;
      }
}

director shield_sjc_ca_us random {
   
   .retries = 2;
   {
    .backend = shield_cache_sjc3120_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3121_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3122_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3123_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3124_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3125_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3126_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3127_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3128_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3129_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3130_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3131_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3132_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3133_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3134_SJC;
    .weight  = 100;
   }{
    .backend = shield_cache_sjc3135_SJC;
    .weight  = 100;
   }
}







# custom acl
## included in top of main

# Who is allowed access ...
acl goodguys {
    "localhost";
    "38.99.32.0"/24; /* 501 Second */
    "206.80.3.0"/26; /* 501 Second */
    "206.80.4.64"/26; /* 501 Second */
    "70.42.185.0"/24; /* 365 Main */
    "23.235.32.0"/20;	/* FASTLY */
    "43.249.72.0"/22;	/* FASTLY */
    "103.244.50.0"/24;	/* FASTLY */
    "103.245.222.0"/23;	/* FASTLY */
    "103.245.224.0"/24;	/* FASTLY */
    "104.156.80.0"/20;	/* FASTLY */
    "157.52.64.0"/18;	/* FASTLY */
    "185.31.16.0"/22;	/* FASTLY */
    "199.27.72.0"/21;	/* FASTLY */
    "202.21.128.0"/24;	/* FASTLY */
    "203.57.145.0"/24;	/* FASTLY */
}


sub vcl_recv {
# custom acl
## Include in vcl_recv

# limit purging via goodguys acl
  if (req.request ~ "PURGE" && ! (client.ip ~ goodguys)) {
    error 403 "Forbidden";
  }


# Removed Referral checks per  WEBOPSM-1377
#include "referer.vcl" ;


#--FASTLY RECV BEGIN
  if (req.restarts == 0) {
    if (!req.http.X-Timer) {
      set req.http.X-Timer = "S" time.start.sec "." time.start.usec_frac;
    }
    set req.http.X-Timer = req.http.X-Timer ",VS0";
  }

            

    
  # default conditions
  set req.backend = F_addr_70_42_185_11;
  

  
  # end default conditions

  # Request Condition: idge.staticworld.net Prio: 10
  if( req.http.host == "idge.staticworld.net" ) {
        
        
    
       if (!req.http.Fastly-FF) {
         if (req.http.X-Forwarded-For) {
           set req.http.Fastly-Temp-XFF = req.http.X-Forwarded-For ", " client.ip;
         } else {
           set req.http.Fastly-Temp-XFF = client.ip;
         }
       } else {
         set req.http.Fastly-Temp-XFF = req.http.X-Forwarded-For;
       }
        
  
  
    set req.grace = 60s; 
    set req.http.host = "idge.staticworld.net";      
  
    
  }
  #end condition
  
      #do shield here F_addr_70_42_185_11 > shield_sjc_ca_us;

    
  
  
  {
    if (req.backend == F_addr_70_42_185_11 && req.restarts == 0) {
      if (server.identity !~ "-SJC$" && req.http.Fastly-FF !~ "-SJC") {
        set req.backend = shield_sjc_ca_us;
      }
      if (!req.backend.healthy) {
        # the shield datacenter is broken so dont go to it
        set req.backend = F_addr_70_42_185_11;
      }
    }
  }
    
  
#--FASTLY RECV END

    if (req.request != "HEAD" && req.request != "GET" && req.request != "FASTLYPURGE") {
      return(pass);
    }

# Custom Cookie Detection
# include "cookie_redirect.vcl";

# Custom Argument Whitelist
# include "argument_whitelist.vcl";

# Custom rules for rss feeds
# include "rss.vcl";


    return(lookup);
}


sub vcl_fetch {



#--FASTLY FETCH BEGIN


# record which cache ran vcl_fetch for this object and when
  set beresp.http.Fastly-Debug-Path = "(F " server.identity " " now.sec ") " if(beresp.http.Fastly-Debug-Path, beresp.http.Fastly-Debug-Path, "");

# generic mechanism to vary on something
  if (req.http.Fastly-Vary-String) {
    if (beresp.http.Vary) {
      set beresp.http.Vary = "Fastly-Vary-String, "  beresp.http.Vary;
    } else {
      set beresp.http.Vary = "Fastly-Vary-String, ";
    }
  }
  
    
  
 # priority: 0

      
    # default
    set beresp.ttl = 3600s;
    set beresp.grace = 86400s;
    return(deliver);
  
 
 
      
  # Gzip default
  if ((beresp.status == 200 || beresp.status == 404) && (beresp.http.content-type ~ "^(text\/html|application\/x\-javascript|text\/css|application\/javascript|text\/javascript|application\/json|application\/vnd\.ms\-fontobject|application\/x\-font\-opentype|application\/x\-font\-truetype|application\/x\-font\-ttf|application\/xml|font\/eot|font\/opentype|font\/otf|image\/svg\+xml|image\/vnd\.microsoft\.icon|text\/plain|text\/xml)\s*($|;)" || req.url ~ "\.(css|js|html|eot|ico|otf|ttf|json)($|\?)" ) ) {
  
    # always set vary to make sure uncompressed versions dont always win
    if (!beresp.http.Vary ~ "Accept-Encoding") {
      if (beresp.http.Vary) {
        set beresp.http.Vary = beresp.http.Vary ", Accept-Encoding";
      } else {
         set beresp.http.Vary = "Accept-Encoding";
      }
    }
    if (req.http.Accept-Encoding == "gzip") {
      set beresp.gzip = true;
    }
  }
 
      
#--FASTLY FETCH END


# Remove all cookies from origin response
unset beresp.http.Set-Cookie;

 if ((beresp.status == 500 || beresp.status == 503) && req.restarts < 1 && (req.request == "GET" || req.request == "HEAD")) {
   restart;
 }

 # New code to get Stream On Miss set sooner, in case we return(deliver) before it happens farther down
 if (req.url.ext ~ "mp4|mov|flv|m4v|ogg") {
   set beresp.do_stream = true;
 }

 if(req.restarts > 0 ) {
   set beresp.http.Fastly-Restarts = req.restarts;
 }

 if (beresp.http.Set-Cookie) {
   set req.http.Fastly-Cachetype = "SETCOOKIE";
   return (pass);
 }

 if (beresp.http.Cache-Control ~ "private") {
   set req.http.Fastly-Cachetype = "PRIVATE";
	set beresp.ttl = 0s;
   set beresp.grace = 0s;
   return (deliver);
   #return (pass);
 }

 if (beresp.status == 500 || beresp.status == 503) {
   set req.http.Fastly-Cachetype = "ERROR";
   set beresp.ttl = 1s;
   set beresp.grace = 5s;
   return (deliver);
 }

  if (beresp.http.Expires || beresp.http.Surrogate-Control ~ "max-age" || beresp.http.Cache-Control ~"s-maxage" || beresp.http.Cache-Control ~"maxage" ) {
    # keep the ttl here
  } else {
    # apply the default ttl
    set beresp.ttl = 3600s;
  }



  if (req.url.ext ~ "mp4|mov|flv|m4v|ogg") {
   set beresp.do_stream = true;
   return(deliver);
}
  return(deliver);
}

sub vcl_hit {
#--FASTLY HIT BEGIN

# we cannot reach obj.ttl and obj.grace in deliver, save them when we can in vcl_hit
  set req.http.Fastly-Tmp-Obj-TTL = obj.ttl;
  set req.http.Fastly-Tmp-Obj-Grace = obj.grace;

  {
    set req.http.Fastly-Cachetype = "HIT";

    
  }
#--FASTLY HIT END



  if (!obj.cacheable) {
    return(pass);
  }
  return(deliver);
}

sub vcl_miss {
#--FASTLY MISS BEGIN
  

# this is not a hit after all, clean up these set in vcl_hit
  unset req.http.Fastly-Tmp-Obj-TTL;
  unset req.http.Fastly-Tmp-Obj-Grace;

  {
    if (req.http.Fastly-Check-SHA1) {
       error 550 "Doesnt exist";
    }
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


 #;

    set req.http.Fastly-Cachetype = "MISS";

    
  }
#--FASTLY MISS END
  return(fetch);
}

sub vcl_deliver {




#--FASTLY DELIVER BEGIN

# record the journey of the object, expose it only if req.http.Fastly-Debug.
  if (req.http.Fastly-Debug || req.http.Fastly-FF) {
    set resp.http.Fastly-Debug-Path = "(D " server.identity " " now.sec ") "
       if(resp.http.Fastly-Debug-Path, resp.http.Fastly-Debug-Path, "");

    set resp.http.Fastly-Debug-TTL = if(obj.hits > 0, "(H ", "(M ")
       server.identity
       if(req.http.Fastly-Tmp-Obj-TTL && req.http.Fastly-Tmp-Obj-Grace, " " req.http.Fastly-Tmp-Obj-TTL " " req.http.Fastly-Tmp-Obj-Grace " ", " - - ")
       if(resp.http.Age, resp.http.Age, "-")
       ") "
       if(resp.http.Fastly-Debug-TTL, resp.http.Fastly-Debug-TTL, "");

    set resp.http.Fastly-Debug-Digest = digest.hash_sha256(req.digest);
  } else {
    unset resp.http.Fastly-Debug-Path;
    unset resp.http.Fastly-Debug-TTL;
  }

  # add or append X-Served-By/X-Cache(-Hits)
  {

    if(!resp.http.X-Served-By) {
      set resp.http.X-Served-By  = server.identity;
    } else {
      set resp.http.X-Served-By = resp.http.X-Served-By ", " server.identity;
    }

    set resp.http.X-Cache = if(resp.http.X-Cache, resp.http.X-Cache ", ","") if(fastly_info.state ~ "HIT($|-)", "HIT", "MISS");

    if(!resp.http.X-Cache-Hits) {
      set resp.http.X-Cache-Hits = obj.hits;
    } else {
      set resp.http.X-Cache-Hits = resp.http.X-Cache-Hits ", " obj.hits;
    }

  }

  if (req.http.X-Timer) {
    set resp.http.X-Timer = req.http.X-Timer ",VE" time.elapsed.msec;
  }

  # VARY FIXUP
  {
    # remove before sending to client
    set resp.http.Vary = regsub(resp.http.Vary, "Fastly-Vary-String, ", "");
    if (resp.http.Vary ~ "^\s*$") {
      unset resp.http.Vary;
    }
  }
  unset resp.http.X-Varnish;


  # Pop the surrogate headers into the request object so we can reference them later
  set req.http.Surrogate-Key = resp.http.Surrogate-Key;
  set req.http.Surrogate-Control = resp.http.Surrogate-Control;

  # If we are not forwarding or debugging unset the surrogate headers so they are not present in the response
  if (!req.http.Fastly-FF && !req.http.Fastly-Debug) {
    unset resp.http.Surrogate-Key;
    unset resp.http.Surrogate-Control;
  }

  if(resp.status == 550) {
    return(deliver);
  }
  

  #default response conditions
    
      
      

  # Response Condition: Third party access Prio: 10
  if( req.http.referer ~ "arnnet\.com\.au|cio\.com\.au|cmo\.com\.au|computerworld\.com\.au|csoonline\.com\.au|goodgearguide\.com\.au|idg\.com\.au|macworld\.com\.au|pcworld\.com\.au|techworld\.com\.au|channelworld\.in|cio\.in|pcworld.\in|idgnews.\in|computerworld\.in" ) {
        
    # s3 s3_fastlylog_static_prod
    log {"syslog 6UFFiCnKbSDy34Yt6gOeGc s3-fastlylog-static-prod :: "} req.http.Fastly-Client-IP {" "} {""-""} {" "} {""-""} {" "} now {" "} req.request {" "} req.url {" "} resp.status;
    
  }
  
#--FASTLY DELIVER END

  return(deliver);
}

sub vcl_error {
#--FASTLY ERROR BEGIN

  if (obj.status == 801) {
     set obj.status = 301;
     set obj.response = "Moved Permanently";
     set obj.http.Location = "https://" req.http.host req.url;
     synthetic {""};
     return (deliver);
  }

      
      
  if (req.http.Fastly-Restart-On-Error) {
    if (obj.status == 503 && req.restarts == 0) {
      restart;
    }
  }

  {
    if (obj.status == 550) {
      return(deliver);
    }
  }
#--FASTLY ERROR END


# custom error
# include "custom_error.vcl";
}

sub vcl_pass {
#--FASTLY PASS BEGIN
  

  {
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


 #;
    set req.http.Fastly-Cachetype = "PASS";
  }

#--FASTLY PASS END
}

sub vcl_log {
# Custom Log
# include "logging.vcl";
}

sub vcl_pipe {
#--FASTLY PIPE BEGIN
  {
     
    
#--FASTLY BEREQ BEGIN
    {
      if (req.http.Fastly-Original-Cookie) {
        set bereq.http.Cookie = req.http.Fastly-Original-Cookie;
      }
      
      if (req.http.Fastly-Original-URL) {
        set bereq.url = req.http.Fastly-Original-URL;
      }
      {
        if (req.http.Fastly-FF) {
          set bereq.http.Fastly-Client = "1";
        }
      }
      {
        # do not send this to the backend
        unset bereq.http.Fastly-Original-Cookie;
        unset bereq.http.Fastly-Original-URL;
        unset bereq.http.Fastly-Vary-String;
        unset bereq.http.X-Varnish-Client;
      }
      if (req.http.Fastly-Temp-XFF) {
         if (req.http.Fastly-Temp-XFF == "") {
           unset bereq.http.X-Forwarded-For;
         } else {
           set bereq.http.X-Forwarded-For = req.http.Fastly-Temp-XFF;
         }
         # unset bereq.http.Fastly-Temp-XFF;
      }
    }
#--FASTLY BEREQ END


    #;
    set req.http.Fastly-Cachetype = "PIPE";
    set bereq.http.connection = "close";
  }
#--FASTLY PIPE END

}

sub vcl_hash {

  #--FASTLY HASH BEGIN

        
  
  #if unspecified fall back to normal
  {
    

    set req.hash += req.url;
    set req.hash += req.http.host;
    set req.hash += "#####GENERATION#####";
    return (hash);
  }
  #--FASTLY HASH END


}

